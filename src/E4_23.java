/////////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/25/19
// This program will create a soda can fixed height and radius.
// It will calculate and print the volume and surface area.
// Then it will prompt the user to enter two values for the
// height and radius of a second soda can. Then it will calculate
// and print the volume and surface area.
/////////////////////////////////////////////////////////////////

import java.util.Scanner;               // importing package to use scanner

public class E4_23 {
    // creating SodaCan class
    public static class SodaCan {
        private double height;          // initializing attribute height
        private double diameter;          // initializing attribute radius
        // constructor that takes user input
        public SodaCan() {
            setHeight();        // calling method to set height by user input
            setDiameter();        // calling method to set radius by user input
        }
        // constructor with fixed height and radius
        public SodaCan(double h, double d) {
            height = h;
            diameter = d;
            // printing height and radius of fixed soda can
            System.out.println("The height of this soda can is " + height
                    + " and the diameter is " + diameter);
        }
        // method to set height by user input
        private void setHeight() {
            // prompting user to enter a height
            System.out.println("Please enter a number to represent the height of the soda can: ");
            // saving number to attribute height
            Scanner in = new Scanner(System.in);
            height = in.nextDouble();
        }
        // method to set radius by user input
        private void setDiameter() {
            // prompting user to enter a radius
            System.out.println("Please enter a number to represent the diameter of the soda can: ");
            // saving number to attribute radius
            Scanner in = new Scanner(System.in);
            diameter = in.nextDouble();
        }
        // method to calculate and print volume
        public void getVolume() {
            // printing the calculated volume
            System.out.println("The volume of the soda can is: " +
                    (3.14 * height * Math.pow(diameter/2, 2)));
        }
        // method to calculate and print surface area
        public void getSurfaceArea() {
            // printing the calculated surface area
            System.out.println("The surface area of the soda can is: " +
                    ((3.14 * height * diameter) + (6.28 * Math.pow(diameter/2, 2))));
        }
    }
    public static void main(String[] args) {
        // initialing first soda can using fixed height and radius
        SodaCan sodaCan = new SodaCan(5.5, 2.55);
        // calculating and printing volume and surface area
        sodaCan.getVolume();
        sodaCan.getSurfaceArea();
        // printing empty lines for spacing
        System.out.println("\n\n");
        // initializing second soda can with user input for height and radius
        SodaCan sodaCan2 = new SodaCan();
        // calculating and printing volume and surface area
        sodaCan2.getVolume();
        sodaCan2.getSurfaceArea();
    }
}
